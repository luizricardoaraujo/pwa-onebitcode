require 'test_helper'

class PagesControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
  end

  test "should get blog" do
    get :blog
    assert_response :success
  end

  test "should get courses" do
    get :courses
    assert_response :success
  end

  test "should get bootcamp" do
    get :bootcamp
    assert_response :success
  end

end
